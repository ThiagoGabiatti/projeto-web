Feature('login');

Scenario('Login com sucesso',  ({ I }) => {
    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login');
    I.fillField('#user', 'teste@teste.com.br')
    I.fillField('#password', '123456')
    I.click('#btnLogin');

}).tag('@sucesso')

Scenario('Tentando logar apenas com e-mail',  ({ I }) => {
    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login');
    I.fillField('#user', 'teste@teste.com.br')
    I.fillField('#password', '')
    I.click('#btnLogin');
    I.waitForText('Senha inválida', 5);

});

Scenario('Tentando logar apenas com senha',  ({ I }) => {
    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login');
    I.fillField('#user', '')
    I.fillField('#password', '123456')
    I.click('#btnLogin'); 
    I.waitForText('E-mail inválido', 5);

})